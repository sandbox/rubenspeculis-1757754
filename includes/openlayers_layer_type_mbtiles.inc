<?php
/**
 * @file
 * mbtiles Layer Type
 */

/**
 * Define the Ctools plugin options.
 */
$plugin = array(
  'title' => t('mbtiles'),
  'description' => t('mbtiles layer type'),
  'layer_type' => array(
    'file' => 'openlayers_layer_type_mbtiles.inc',
    'class' => 'openlayers_layer_type_mbtiles',
    'parent' => 'openlayers_layer_type',
  ),
);

/**
 * OpenLayers MapTiler Layer Type class
 */
class openlayers_layer_type_mbtiles extends openlayers_layer_type {

  function __construct($layer = array(), $map = array()) {
    parent::__construct($layer, $map);
    if (isset($this->data)) {
      $this->data += $this->options_init();
      $this->data['baselayer'] = $this->data['isBaseLayer'];
    }
    else {
      $this->data = $this->options_init();
    }
  }

  /**
   * Provide initial values for options.
   */
  function options_init() {
    return array(
      'serverResolutions' => openlayers_get_resolutions('900913'),
      'maxExtent' => openlayers_get_extent('900913'),
      'projection' => array('900913'),
      'baselayer' => TRUE,
      'type' => 'png',
      'layer_handler' => 'mbtiles',
    );
  }

  /**
   * Options form which generates layers
   */
  function options_form() {
    return array(
      'base_url' => array(
        '#type' => 'textfield',
        '#title' => t('Base url'),
        '#default_value' => isset($this->data['base_url']) ? 
            $this->data['base_url'] : ''
      ),
      'flip' => array(
        '#type' => 'checkbox',
        '#default_value' => isset($this->data['flip']) ? 
            $this->data['flip'] : TRUE,
        '#title' => t('Flipped y-axis'),
        '#description' => t('Is this a mbtiles databse?'),
      ),
      'isBaseLayer' => array(
        '#type' => 'checkbox',
        '#default_value' => isset($this->data['isBaseLayer']) ?
            $this->data['isBaseLayer'] : TRUE,
        '#title' => t('BaseLayer'),
        '#description' => t('Uncheck to make this map an overlay')
      ),
      'resolutions' => array(
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => array_combine(
          array_map('strval', openlayers_get_resolutions('900913')),
          range(0, 18)
        ),
        '#title' => t('Zoom range'),
        '#default_value' => isset($this->data['resolutions']) ?
          $this->data['resolutions'] :
          array_map('strval', openlayers_get_resolutions('900913'))
      ),
      'layer_type' => array(
        '#type' => 'hidden',
        '#value' => 'openlayers_layer_type_mbtiles'
      ),
    );
  }

  /**
   * Render.
   */
  function render(&$map) {
    drupal_add_js(drupal_get_path('module', 'mbtiles_for_openlayers') .
      '/plugins/layer_types/openlayers_layer_type_mbtiles.js');
    return $this->options;
  }
}
